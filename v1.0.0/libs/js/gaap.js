// gaap.js
// Version: 1.0.0
// Copyright 2015 Genband
// ----------------------
// UMD module definition as described by https://github.com/umdjs/umd
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(factory);
    } else {
        // Browser globals
        root.gaap = root.GappAPI = factory.apply({});
    }
 }(this, function () {
    'use strict';

    var gaapVersion = '1.0.0';

    // UMD module definition as described by https://github.com/umdjs/umd
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(factory);
    } else {
        // Browser globals
        root.vvd = factory();
    }
 }(this, function () {

var ContactsModule = function()
{

}

vvd.contactsmodule = new ContactsModule();

var AlertsModule = function()
{

}

vvd.alertsmodule = new AlertsModule();



}))}));